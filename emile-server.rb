require 'sinatra'
require 'sinatra/activerecord'
require 'sinatra/cors'
require 'ffi-rzmq'

@environment = ENV['RACK_ENV'] || 'development'
@dbconfig = YAML.load(ERB.new(File.read('db/config.yml')).result)
ActiveRecord::Base.establish_connection @dbconfig[@environment]

$context = ZMQ::Context.new
$publisher = $context.socket(ZMQ::PUB)
$publisher.connect("tcp://emile.sandroandrade.org:5559")
puts $publisher

set :allow_origin, "*"
set :allow_methods, "GET,HEAD,POST"
set :allow_headers, "content-type,if-modified-since"
set :expose_headers, "location,link"

current_dir = Dir.pwd
Dir["#{current_dir}/plugins/*.rb"].each { |file| require file }

