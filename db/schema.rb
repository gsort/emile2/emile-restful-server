# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_27_181300) do

  create_table "categories", force: :cascade do |t|
    t.string "name"
  end

  create_table "emilets", force: :cascade do |t|
    t.string "name"
  end

  create_table "emilets_categories", id: false, force: :cascade do |t|
    t.integer "emilet_id", null: false
    t.integer "category_id", null: false
  end

  create_table "match_players", force: :cascade do |t|
    t.string "player_name"
    t.string "player_email"
    t.boolean "approved"
    t.integer "score"
    t.integer "match_id"
    t.index ["match_id"], name: "index_match_players_on_match_id"
  end

  create_table "matches", force: :cascade do |t|
    t.string "description"
    t.string "creator_name"
    t.string "creator_email"
    t.integer "emilet_id"
    t.integer "status"
    t.integer "topic"
    t.index ["emilet_id"], name: "index_matches_on_emilet_id"
    t.index ["topic"], name: "index_matches_on_topic"
  end

  create_table "messages", force: :cascade do |t|
    t.string "topic"
    t.string "topic_name"
    t.string "sender_name"
    t.string "sender_avatar"
    t.string "title"
    t.text "body"
    t.string "term"
    t.datetime "sent_at"
    t.index ["topic"], name: "index_messages_on_topic"
  end

  create_table "preferences", force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.index ["key"], name: "index_preferences_on_key"
  end

  create_table "question_options", force: :cascade do |t|
    t.string "description"
    t.integer "question_id"
    t.index ["question_id"], name: "index_question_options_on_question_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "category_id"
    t.integer "right_option"
    t.boolean "approved"
    t.index ["category_id"], name: "index_questions_on_category_id"
  end

  add_foreign_key "match_players", "matches"
  add_foreign_key "matches", "emilets"
  add_foreign_key "question_options", "questions"
  add_foreign_key "questions", "categories"
end
