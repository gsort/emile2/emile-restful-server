class CreatePreferences < ActiveRecord::Migration[5.0]
  def change
    create_table :preferences do |t|
      t.string :key, index: true
      t.string :value
    end
  end
end
