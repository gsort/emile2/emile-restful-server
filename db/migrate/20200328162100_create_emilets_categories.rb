class CreateEmiletsCategories < ActiveRecord::Migration[5.0]
  def change
    create_join_table :emilets, :categories, table_name: :emilets_categories
  end
end
