class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.string :name
      t.string :description
      t.references :category, index: true, foreign_key: true
      t.integer :right_option
      t.boolean :approved
    end
  end
end
