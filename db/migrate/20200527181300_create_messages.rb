class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.string :topic, index: true
      t.string :topic_name
      t.string :sender_name
      t.string :sender_avatar
      t.string :title
      t.text :body
      t.string :term
      t.datetime :sent_at
    end
  end
end
