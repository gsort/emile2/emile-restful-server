class CreateMatches < ActiveRecord::Migration[5.0]
  def change
    create_table :matches do |t|
      t.string :description
      t.string :creator_name
      t.string :creator_email
      t.references :emilet, index: true, foreign_key: true
      t.integer :status
      t.integer :topic, index: true
    end
  end
end
