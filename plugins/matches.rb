require 'active_record'

class Match < ActiveRecord::Base
  belongs_to :emilet
  has_many :match_players, :dependent => :destroy
end

class MatchPlayer < ActiveRecord::Base
  belongs_to :match
end

get '/matches_by_type/:type' do
  if params[:type] == '0'
    Match.where(status: 0..1).order(:id).to_json(:include => [ :emilet, :match_players ])
  elsif params[:type] == '1'
    Match.where(status: 2).order(:id).to_json(:include => [ :emilet, :match_players ])
  end
end

get '/matches/:id' do
  Match.find(params[:id]).to_json(:include => [ :match_players ])
end

post '/emilets/:id/matches/:single_return' do
  payload = JSON.parse(request.body.read)
  match = Match.new
  match.description = payload['description']
  match.emilet = Emilet.find(params[:id])
  match.status = 0
  topic = -1
  loop do
    topic = rand(1..50000)
    break if !Match.find_by({ topic: topic, status: 0..1 })
  end
  match.topic = topic
  match.creator_name = payload['nickname']
  match.creator_email = payload['email']
  match.match_players << MatchPlayer.create(player_name: payload['nickname'], player_email: payload['email'], approved: true, score: 0)
  match.save
  $publisher.send_string('matches', ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "refresh_matches" }')
  if params[:single_return] == '1'
    match.to_json(:include => [ :match_players ])
  else
    Match.where(status: 0..1).order(:id).to_json(:include => [ :emilet, :match_players ])
  end
end

put '/matches/:id/:single_return' do
  payload = JSON.parse(request.body.read)
  match = Match.find(params[:id])
  statusBefore = match.status
  match.status = payload['status']
  match.save
  $publisher.send_string('matches', ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "refresh_matches" }')
  if statusBefore = 0 && match.status = 1
    $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
    $publisher.send_string('{ "action": "match_started" }')
  end
  if params[:single_return] == '1'
    match.to_json(:include => [ :match_players ])
  else
    Match.where(status: 0..1).order(:id).to_json(:include => [ :emilet, :match_players ])
  end
end

delete '/matches/:id' do
  match = Match.find(params[:id])
  $publisher.send_string('matches', ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "refresh_matches" }')
  $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "match_aborted" }')
  match.destroy
  Match.where(status: 0..1).order(:id).to_json(:include => [ :emilet, :match_players ])
end

post '/matches/:id/players/:single_return' do
  payload = JSON.parse(request.body.read)
  match = Match.find(params[:id])
  match.match_players << MatchPlayer.create(player_name: payload['nickname'], player_email: payload['email'], score: 0)
  match.save
  $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "new_guest", "nickname": "' + payload['nickname'] + '" }')
  if params[:single_return] == '1'
    match.to_json(:include => [ :match_players ])
  else
    Match.where(status: 0..1).order(:id).to_json(:include => [ :emilet, :match_players ])
  end
end

put '/matches/:mid/players/:pid/:single_return' do
  payload = JSON.parse(request.body.read)
  match = Match.find(params[:mid])
  player = MatchPlayer.find(params[:pid])
  if payload['nickname'] == match.creator_name
    if payload['approved'] == true
      player.approved = true
      player.save
      $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
      $publisher.send_string('{ "action": "guest_accepted", "nickname": "' + player.player_name + '" }')
    else
      $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
      $publisher.send_string('{ "action": "guest_rejected", "nickname": "' + player.player_name + '" }')
      player.destroy
    end
    if params[:single_return] == '1'
      match.to_json(:include => [ :match_players ])
    else
      Match.where(status: 0..1).order(:id).to_json(:include => [ :emilet, :match_players ])
    end
  else
    '{ "error": "Somente o criador da partida pode aprovar novos jogadores!" }'
  end
end

delete '/matches/:mid/players/:pid' do
  match = Match.find(params[:mid])
  player = MatchPlayer.find(params[:pid])
  $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "guest_giveup", "nickname": "' + player.player_name + '" }')
  player.destroy
  Match.where(status: 0..1).order(:id).to_json(:include => [ :emilet, :match_players ])
end
