require 'active_record'

class Category < ActiveRecord::Base
  has_many :questions, :dependent => :destroy
  has_and_belongs_to_many :emilets, join_table: "emilets_categories"
end

get '/categories' do
  Category.all.order(:id).to_json
end

get '/categories/:id' do
  Category.find(params[:id]).to_json
end

post '/categories/:single_return' do
  payload = JSON.parse(request.body.read)
  category = Category.create(name: payload['name'])
  if params[:single_return] == '1'
    category.to_json
  else
    Category.all.order(:id).to_json
  end
end

put '/categories/:id/:single_return' do
  payload = JSON.parse(request.body.read)
  category = Category.find(params[:id])
  category.name = payload['name']
  category.save
  if params[:single_return] == '1'
    category.to_json
  else
    Category.all.order(:id).to_json
  end
end

delete '/categories/:id' do
  Category.find(params[:id]).destroy
  Category.all.order(:id).to_json
end
