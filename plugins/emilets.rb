require 'active_record'

class Emilet < ActiveRecord::Base
  has_and_belongs_to_many :categories, join_table: "emilets_categories", :dependent => :destroy
  has_many :matches, :dependent => :destroy
end

get '/emilets' do
  Emilet.all.order(:id).to_json(:include => [ :categories ])
end

get '/emilets/:id' do
  Emilet.find(params[:id]).to_json
end

get '/emilets/:id/questions' do
  Question.joins(category: :emilets).where(emilets: {id: params[:id]}, approved: true).to_json(:include => [ :question_options ])
end

post '/emilets/:single_return' do
  payload = JSON.parse(request.body.read)
  emilet = Emilet.create(name: payload['name'])
  payload['categories'].each do |category|
    emilet.categories << Category.find(category['id'])
  end
  emilet.save
  if params[:single_return] == '1'
    emilet.to_json(:include => [ :categories ])
  else
    Emilet.all.order(:id).to_json(:include => [ :categories ])
  end
end

put '/emilets/:id/:single_return' do
  payload = JSON.parse(request.body.read)
  emilet = Emilet.find(params[:id])
  emilet.name = payload['name']
  emilet.categories.destroy_all
  payload['categories'].each do |category|
    emilet.categories << Category.find(category['id'])
  end
  emilet.save
  if params[:single_return] == '1'
    emilet.to_json(:include => [ :categories ])
  else
    Emilet.all.order(:id).to_json(:include => [ :categories ])
  end
end

delete '/emilets/:id' do
  Emilet.find(params[:id]).destroy
  Emilet.all.order(:id).to_json(:include => [ :categories ])
end
