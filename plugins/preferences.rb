require 'active_record'

class Preferences < ActiveRecord::Base
end

get '/preferences/:key' do
  Preferences.find_by({ key: params[:key] }).to_json
end
