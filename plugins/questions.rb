require 'active_record'

class Question < ActiveRecord::Base
  belongs_to :category
  has_many :question_options, :dependent => :destroy
end

class QuestionOption < ActiveRecord::Base
  belongs_to :question
end

get '/categories/:id/questions' do
  Category.find(params[:id]).questions().order(:id).to_json(:include => [ :question_options ])
end

get '/questions/:id' do
  Question.find(params[:id]).to_json(:include => [ :question_options ])
end

get '/pending_questions' do
  Question.where(approved: false).order(:id).to_json(:include => [ :category, :question_options ])
end

post '/categories/:id/questions/:single_return' do
  payload = JSON.parse(request.body.read)
  question = Question.new
  question.name = payload['name']
  question.description = payload['description']
  question.category = Category.find(params[:id])
  question.right_option = payload['right_option']
  question.approved = false
  payload['question_options'].each do |option|
    question.question_options << QuestionOption.create(description: option['description'])
  end
  question.save
  if params[:single_return] == '1'
    question.to_json(:include => [ :question_options ])
  else
    question.category.questions().order(:id).to_json(:include => [ :question_options ])
  end
end

put '/categories/:cid/questions/:qid/:single_return' do
  payload = JSON.parse(request.body.read)
  question = Question.find(params[:qid])
  question.name = payload['name']
  question.description = payload['description']
  question.category = Category.find(params[:cid])
  question.right_option = payload['right_option']
  wasPending = !question.approved
  question.approved = payload['approved']
  question.question_options.destroy_all
  payload['question_options'].each do |option|
    question.question_options << QuestionOption.create(description: option['description'])
  end
  question.save
  if params[:single_return] == '1'
    question.to_json(:include => [ :question_options ])
  else
    if wasPending && question.approved
      Question.where(approved: false).order(:id).to_json(:include => [ :category, :question_options ])
    else
      question.category.questions().order(:id).to_json(:include => [ :question_options ])
    end
  end
end

delete '/questions/:id' do
  question = Question.find(params[:id])
  category = question.category 
  question.destroy
  category.questions().order(:id).to_json(:include => [ :question_options ])
end
