require 'firebase_cloud_messenger'

FirebaseCloudMessenger.credentials_path = ENV["FIREBASE_SVCACCOUNT_FILE"]

post '/notification' do
  payload = JSON.parse(request.body.read)
  message = {
    notification: {
      title: payload['title'],
      body: payload['body']
    },
    topic: payload['topic']
  }
  FirebaseCloudMessenger.send(message: message).to_json
end

post '/notification_new_version/:version' do
  puts params[:version]
  message = {
    notification: {
      title: "Nova versão disponível!",
      body: "Atualize agora para a versão #{params[:version]} e tenha acesso às novidades mais atuais do Emile."
    },
    condition: "!('#{params[:version]}' in topics)",
  }
  FirebaseCloudMessenger.send(message: message).to_json
end
