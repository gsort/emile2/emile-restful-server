require 'active_record'
require 'firebase_cloud_messenger'

FirebaseCloudMessenger.credentials_path = ENV["FIREBASE_SVCACCOUNT_FILE"]

class Message < ActiveRecord::Base
end

class Preferences < ActiveRecord::Base
end

get '/messages/:topic/:lastId' do
  currentTerm = Preferences.find_by({ key: 'currentTerm' }).value
  if params[:lastId] != -1
    Message.where("topic = ? and term = ? and id > ?", params[:topic], currentTerm, params[:lastId]).order(sent_at: :asc).to_json
  else
    Message.where("topic = ? and term = ?", params[:topic], currentTerm).order(sent_at: :asc).to_json
  end
end

post '/messages/' do
  payload = JSON.parse(request.body.read)
  message = Message.new
  message.topic = payload['topic']
  message.topic_name = payload['topic_name']
  message.sender_name = payload['sender_name']
  message.sender_avatar = payload['sender_avatar']
  message.title = payload['title']
  message.body = payload['body']
  message.term = Preferences.find_by({ key: 'currentTerm' }).value
  message.sent_at = DateTime.now
  message.save
  firebaseMessage = {
    notification: {
      title: payload['topic_name'],
      body: payload['title']
    },
    data: {
      "channelId": payload['topic'],
      "channelDescription": payload['topic_name']
    },
    topic: payload['topic']
  }
  FirebaseCloudMessenger.send(message: firebaseMessage)
  message.to_json
end
