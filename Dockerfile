FROM ruby:alpine

LABEL maintainer="sandroandrade@ifba.edu.br"

RUN apk update && \
    apk upgrade && \
    apk add build-base mariadb-dev sqlite-dev libzmq && \
    rm -rf /var/cache/apk/* && \
    ln -s /usr/lib/libzmq.so.5 /usr/lib/libzmq.so

# Set environment variables.
ENV HOME /var/app
ENV FIREBASE_SVCACCOUNT_FILE /var/app/emile-client-firebase-adminsdk-npj33-d2ddf0f161.json

# Define working directory.
COPY . /var/app
RUN cd /var/app && bundle install

WORKDIR /var/app

EXPOSE 3000

CMD ["/bin/sh", "/var/app/bin/start.sh"]
